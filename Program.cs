﻿//Esercizio 1:
//Crea un programma che prenda in input un numero e lo stampi.Tutto ciò viene ripetuto finchè l'utente non inserisce un numero multiplo di 3 durante la fase di inserimento

using System;

namespace Eserciziomultiplotre
{
    class Program
    {
        static void Main(string[] args)
        {
            int numeroDaInserire;
            Console.WriteLine("Ciao bomber, inserisci un numero e te lo stamperò, forse.");
            do
            {
                Console.WriteLine("Inserisci il numero che vuoi far stampare al tuo Re: ");
                numeroDaInserire = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(numeroDaInserire);
            }
            while (numeroDaInserire % 3 != 0);
        }
    }
}
